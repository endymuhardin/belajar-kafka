package com.muhardin.endy.belajar.kafka.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muhardin.endy.belajar.kafka.dto.TagihanResponse;

import lombok.extern.slf4j.Slf4j;

@Service @Slf4j
public class KafkaListenerService {
    @Autowired private ObjectMapper objectMapper;

    @KafkaListener(topics = "${kafka.topic.tagihan-response}")
    public void handleTagihanResponse(String message) throws JsonMappingException, JsonProcessingException{
        TagihanResponse response = objectMapper.readValue(message, TagihanResponse.class);
        log.info("Menerima tagihan response : {}", response.toString());
    }
}

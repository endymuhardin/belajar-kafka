package com.muhardin.endy.belajar.kafka.dto;

import lombok.Data;

@Data
public class VirtualAccount {
    private String bank;
    private String nomor;
}

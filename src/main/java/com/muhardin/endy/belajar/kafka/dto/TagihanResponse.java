package com.muhardin.endy.belajar.kafka.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class TagihanResponse {
    private String debitur;
    private String jenisTagihan;
    private BigDecimal nilai;
    private String nomorTagihan;
    private List<VirtualAccount> virtualAccounts = new ArrayList<>();
}
